<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>example.jsp</title>
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">    
        <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
        <meta http-equiv="description" content="This is my page">
        <!--
        <link rel="stylesheet" type="text/css" href="styles.css">
        -->
        
        <script type="text/javascript">
                function exportExcel(){
                        var frm = document.exportForm;
                        var toExcelStr = document.getElementById('export_table').innerHTML;
                        alert("JavaScript");
                        frm.elements['content'].value = toExcelStr;
                        frm.elements['title'].value = 'SheetExcel.xls';
                        frm.submit();
                }
        </script>
  </head>
  
  <body>

    <table  border="1" cellspacing="0" cellpadding="0">
        <tr id="export_table">
                <th> The first column  </th>
                <th> The second column  </th>
                <th> The third column  </th>
                <th> The fourth column  </th>
                <th> The fifth column  </th>
        </tr>
        <tr>
                <td>111</td>
                <td>222</td>
                <td>333</td>
                <td>444</td>
                <td>555</td>
        </tr>
        <tr>
                <td>111</td>
                <td>222</td>
                <td>333</td>
                <td>444</td>
                <td>555</td>
        </tr>
        <tr>
                <td>111</td>
                <td>222</td>
                <td>333</td>
                <td>444</td>
                <td>555</td>
        </tr>
        <tr>
                <td>111</td>
                <td>222</td>
                <td>333</td>
                <td>444</td>
                <td>555</td>
        </tr>
        <tr>
                <td>111</td>
                <td>222</td>
                <td>333</td>
                <td>444</td>
                <td>555</td>
        </tr>
    </table>
    <form name="exportForm" action="exportToExcel.jsp" method="post">
        <!-- excel File name   -->
        <input name="title" type="text" />
        <!-- excel The contents of a file   -->
        <input name="content" type="text" />
            <input type="submit" value="Export">
        <input type="submit" 
name="B1" value="Submit" />
    </form>
  </body>
</html>